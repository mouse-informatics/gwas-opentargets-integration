The aim of this project is mapping IMPC phenotypes into GWAS hits. 
------------------------------------------------------------------

Genetic Open targets is taking GWAS results from GWAS Catalog and UKBiobank, and expanding the assocaited genes to a SNP by LD blocks). A good pilot use case is looking at mappings for cardiac genes. 

1) Determine if FTP or GraphQL is the best way to extract data
2) Gene list- Violeta best to provide. These are human genes that are one-to-one orthologues to IMPC mouse genes with cardiac phenotypes.
3) Starting with gene list above extract all "traits"*, the SNP variant used to make the association,  whether this was a "lead variant" (from a gwas) or a "tag variant"( inferred by LD), and the underlying study/publication.
4) Assess size of this- if small enough for a excel file great. If too big, extract the unique traits. 
5) Violeta and I will review over August and create a manual mapping to MP

---

# To run opentargets-analysis.py 
(which is found in the cardiac_summary_project directory)

see:
https://docs.python-guide.org/dev/virtualenvs/

## 1. Check your python install

This project was written using python 3.7.4
and takes advantage of some of the 3.5+ language features


Test your python installation:

```
virtualenv --version
```

You should see something like:

```
16.4.3
```

If necessary install virtualenv via pip:

```
pip install virtualenv
```


## 2. Create a virtual environment for a project

```
cd cardiac_summary_project
virtualenv venv
```

## 3. To begin using the virtual environment, it needs to be activated:

```
source venv/bin/activate
```

## 4. Install the packages required for the project

```
pip install -r requirements.txt
```

## 5. Once you have finished working in the virtual environment clean up

```
deactivate
```

This puts you back to the system’s default Python interpreter
with all its installed libraries.