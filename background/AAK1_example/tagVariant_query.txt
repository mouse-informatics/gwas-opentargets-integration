query example {
  tagVariantsAndStudiesForIndexVariant(variantId: "2_69811100_C_G"){
    associations{
      tagVariant {
      	id
      }
      study {
      	studyId
      	traitReported
      	traitEfos
        traitCategory
      }
    }
  }
}
