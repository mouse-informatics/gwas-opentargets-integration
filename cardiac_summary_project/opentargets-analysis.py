import os, sys, csv, requests, json


class DataLoader:
    
    def __init__(self, params):
        
        self.filename = params.get('file')
        
        self.headings = ['Gene', 'Allele', 'Zygosity', 'Sex', 'Life Stage', 'Phenotype', 'Procedure', 'Parameter', 'Phenotyping Center', 'Source', 'P Value', 'Data']
        
        self.headingsMapping = {'Gene' : self.headings.index('Gene'),
                                'Allele' : self.headings.index('Allele'),
                                'Zygosity' : self.headings.index('Zygosity'),
                                'Sex' : self.headings.index('Sex'),
                                'Life Stage' : self.headings.index('Life Stage'),
                                'Phenotype' : self.headings.index('Phenotype'),
                                'Procedure' : self.headings.index('Procedure'),
                                'Parameter' : self.headings.index('Parameter'),
                                'Phenotyping Center' : self.headings.index('Phenotyping Center'),
                                'Source' : self.headings.index('Source'),
                                'P Value' : self.headings.index('P Value'),
                                'Data' : self.headings.index('Data')
                               }
        
        self.mouse_genes = []


    
    def testHeadings(self, row, headings):
        
        
        if row != headings:
            print('The headings of the spreadsheet have changed')
            print('Expected:')
            for index, elem in enumerate(headings):
                print(index, elem)
            print('')
            print('Found:')
            for indexF, elemF in enumerate(row):
                print(indexF, elemF)
            print('')
            print('******************')
            sys.exit('Headers have changed')
    
    
    def readData(self):
        with open(self.filename, newline='') as f:
            reader = csv.reader(f, delimiter='\t')
            try:
                counter=0
                for row in reader:
                    counter+=1
                    
                    # Ensure the expected columns are present
                    if counter == 1:
                        self.testHeadings(row,self.headings)
                    
                    
                    # Load in the data rows
                    elif row[self.headingsMapping["Gene"]]:
                        self.mouse_genes.append(row[self.headingsMapping["Gene"]])
                    
                    
            except csv.Error as e:
                sys.exit('file {}, line {}: {}'.format(self.filename, reader.line_num, e))
    
    
    def uniqueGenes(self):
        return list(set(self.mouse_genes))


class MouseData:
    
    def __init__(self):
        
        self.mouse_genes = []
        self.mouse_ids = []
        self.id_to_entry = {}
        self.id_to_symbol = {}
        self.symbol_to_id = {}
    
    def uniqueIds(self):
        return list(set(self.mouse_ids))




class ValidateMouseGenes:
    
    def __init__(self, params):
        
        self.mouse_genes = params.get('mouse_genes')
        if self.mouse_genes is None:
            self.mouse_genes = []

        self.mouse_data = MouseData()
        self.mouse_data.mouse_genes = self.mouse_genes

    

    def fetchIds(self):
        for gene_name in self.mouse_genes:
            id = self.processNameAsSymbol(gene_name)
            if id:
                self.mouse_data.mouse_ids.append(id)
                self.mouse_data.id_to_entry[id] = gene_name
                self.mouse_data.id_to_symbol[id] = gene_name
                self.mouse_data.symbol_to_id[gene_name] = id
            else:
                id = self.processNameAsSynonym(gene_name)
                if id:
                    self.mouse_data.mouse_ids.append(id)
                    self.mouse_data.id_to_entry[id] = gene_name
                    symbol = self.fetchSymbolForId(id)
                    self.mouse_data.id_to_symbol[id] = symbol
                    self.mouse_data.symbol_to_id[symbol] = id
                else:
                    print("Cannot identify the mouse gene: {:s}".format(gene_name))


    def processNameAsSymbol(self,name):
        query = '{ "query": "{mouse_gene(where: {symbol: {_eq: \\"' + name + '\\"}}) {mgi_gene_acc_id}}" }'
        data = self.fetchDataForQuery(query)
        id = self.extractId(data,"symbol")
        return id

    def extractId(self,json,datatype):
        acc_list=[]
        if datatype == "symbol":
            acc_list = json['data']['mouse_gene']
        else:
            acc_list = json['data']['mouse_gene_synonym']
        
        if len(acc_list) != 1:
            print("Error obtaining the mouse reference data.")
            sys.exit()

        return acc_list[0]['mgi_gene_acc_id']




    def processNameAsSynonym(self,name):
        query = '{ "query": "{mouse_gene_synonym(where: {synonym: {_eq: \\"' + name + '\\"}}) {mgi_gene_acc_id}}" }'
        data = self.fetchDataForQuery(query)
        id = self.extractId(data,"synonym")
        return id


    def fetchDataForQuery(self, query):
        
        url = 'http://193.62.55.22/refdata/v1/graphql'
        
        headers = {'Content-type': 'application/json',
                   'cache-control': 'no-cache'}

         
        response = requests.post(url, headers=headers, data=query)
        
        response.raise_for_status()
        return response.json()
    



    def fetchSymbolForId(self,id):
        query ='{ "query": "{mouse_gene(where: {mgi_gene_acc_id: {_eq: \\"' + id + '\\"}}) {symbol}}" }'
        json = self.fetchDataForQuery(query)
        return json['data']['mouse_gene'][0]['symbol']






class HumanOrthologs:
    
    def __init__(self, params):
        
        self.mouse_data = params.get('mouse_data')
        self.mouse_ids = self.mouse_data.uniqueIds()
        self.working_ids = []
        self.working_support_count = 5
        self.minimum_support_count_threshold = 5
        self.no_orthologs = []
        self.mouse_id_to_human_id_mapping = {}
        self.mouse_symbol_to_human_id_mapping = {}
        self.human_orthologs = {}
        self.debug = False
 

    def setMinimumSupportCountThreshold(self,support_count):
        if (support_count > 0) and (support_count < 13):
            self.minimum_support_count_threshold = support_count


    def findOneToOneOrthologs(self):
        self.findOneToOneOrthologsAboveSupportCountThreshold(self.minimum_support_count_threshold)

           
    def findOneToOneOrthologsAboveSupportCountThreshold(self,support_count):
        self.working_ids = self.mouse_ids.copy()
        self.working_support_count = support_count
        self.minimum_support_count_threshold = support_count

        json = self.fetchOrthologs()
        self.working_ids = []
        self.extractHumanGenesWithSingleOrtholog(json)
    
       
    def findBestOrthologMatches(self):
        self.working_ids = self.mouse_ids.copy()
        for support_count in range(self.minimum_support_count_threshold, 13):
            self.working_support_count = support_count
            json = self.fetchOrthologs()
            self.working_ids = []
            self.extractHumanGenesWithSingleOrtholog(json)



    def fetchOrthologs(self):
        
        if len(self.working_ids) > 0:
            query ='{ "query": "{mouse_gene(where: {mgi_gene_acc_id: {_in: ' + json.dumps(self.working_ids).replace('"','\\"') + '}}) {mgi_gene_acc_id, symbol, orthologs(where: {support_count: {_gt: ' + str(self.working_support_count) + '}}) {human_gene {symbol,hgnc_gene {ensembl_gene_acc_id}}}}}" }'

            url = 'http://193.62.55.22/refdata/v1/graphql'
        
            headers = {'Content-type': 'application/json',
                   'cache-control': 'no-cache'}
        
            response = requests.post(url, headers=headers, data=query)
        
            response.raise_for_status()
            return response.json()
        else:
            return eval("{'data': {'mouse_gene': []}}")


    
    def extractHumanGenesWithSingleOrtholog(self, json):

        mouse_genes = json['data']['mouse_gene']

        if self.debug:
            print()
            print(self.working_support_count)

        for gene in mouse_genes:

            acc = gene['mgi_gene_acc_id']
            symbol = gene['symbol']
            orthologs = gene['orthologs']

            if len(orthologs) == 1:
                human_symbol = orthologs[0]['human_gene']['symbol']
                ensembl_gene_acc = orthologs[0]['human_gene']['hgnc_gene']['ensembl_gene_acc_id']
                self.human_orthologs[ensembl_gene_acc] = human_symbol
                self.mouse_id_to_human_id_mapping[acc] = ensembl_gene_acc
                self.mouse_symbol_to_human_id_mapping[symbol] = ensembl_gene_acc
                if self.debug:
                    print(" Ensembl_Accession_Id: {:s} - Gene symbol: {:s}".format(ensembl_gene_acc,human_symbol))
 
            elif len(orthologs) > 1:
                self.working_ids.append(acc)

            elif (len(orthologs) == 0) and (self.working_support_count == self.minimum_support_count_threshold):          
                 self.no_orthologs.append(acc)



    def summariseOrthologAssignmentIssues(self):
        print("Genes with no orthologs at a support count thresold of: {:d}".format(self.minimum_support_count_threshold))
        print("MGI ID\tMouse Gene Symbol")
        for gene_id in self.no_orthologs:
            symbol = self.mouse_data.id_to_entry[gene_id]
            print("{:s}\t{:s}".format(gene_id,symbol))
        print()
        print("Genes that potentially have multiple orthologs:")
        print("MGI ID\tMouse Gene Symbol")
        for gene_id in self.working_ids:
            symbol = self.mouse_data.id_to_entry[gene_id]
            print("{:s}\t{:s}".format(gene_id,symbol))
        print()


    def reportMouseOrthologMapping(self):

        print("Mouse Gene Entry\tValid Mouse Gene Symbol\tMGI ID\tMapped Human Gene Symbol\tMapped Human Gene ENSEMBL ID")

        for symbol, ensembl_id in sorted(self.mouse_symbol_to_human_id_mapping.items(),key=lambda x: x[0] ):
            
            mgi_id = self.mouse_data.symbol_to_id[symbol]
            entry = self.mouse_data.id_to_entry[mgi_id]
            human_symbol = self.human_orthologs[ensembl_id]

            print("{:s}\t{:s}\t{:s}\t{:s}\t{:s}".format(entry,symbol,mgi_id,human_symbol,ensembl_id))
        print()




class StatsRecorder:
      
    def __init__(self):
        
        self.efos = {}
        self.reportedTraits = {}
        self.categories = {}



    def addEfos(self,traitEfos):
        for efo in traitEfos:
            self.addItemToDictionary(efo,self.efos)

    def addReportedTrait(self,traitReported):
       self.addItemToDictionary(traitReported,self.reportedTraits)

    def addTraitCategory(self,traitCategory):
       self.addItemToDictionary(traitCategory,self.categories)


    def addItemToDictionary(self,item,dictionary):
        if item not in dictionary:
            dictionary[item] = 1
        else:
            dictionary[item] = dictionary[item] + 1



    def efosByFrequency(self):
        self.reportDataByFrequency(self.efos)

    def traitsByFrequency(self):
        self.reportDataByFrequency(self.reportedTraits)

    def traitCategoriesByFrequency(self):
        self.reportDataByFrequency(self.categories)


    def reportDataByFrequency(self,dictionary):
        for key, value in sorted(dictionary.items(),key=lambda x: (-x[1],x[0])):
            print("{:s}\t{:d}".format(key,value) )
        print()







class Analysis:
     
    def __init__(self, params):
        
        self.human_ensembl_acc_ids = params.get('human_ensembl_acc_ids')
        self.summary = StatsRecorder()

        self.variants = []
        self.associated_lead_variants = []
        self.working_associated_lead_variant_set = []
        self.tag_variants = []

        self.variant_studies = {}
        self.variant_study_data = {}

        self.studies = []

    


    def processHumanGenes(self):
        self.obtainAssociatedLeadVariantsAndTagVariants()
        self.obtainColocalisedStudies()
        self.reportStats()


    def processHumanGenesForLeadVariantsOnly(self):
        self.obtainAssociatedLeadVariantsOnly()
        self.obtainColocalisedStudies()
        self.reportStats()


    def processHumanGenesWithOutColocalisedStudies(self):
        self.obtainAssociatedLeadVariantsAndTagVariants()
        self.reportStats()



    def reportStats(self):
        self.summary.efosByFrequency()
        self.summary.traitsByFrequency()
        self.summary.traitCategoriesByFrequency()





    def obtainAssociatedLeadVariantsAndTagVariants(self):
        for id in self.human_ensembl_acc_ids:
            self.working_associated_lead_variant_set = []
            query ='{ "query": "{studiesAndLeadVariantsForGene(geneId: \\"' + id + '\\"){indexVariant{id}study {studyId,traitReported,traitEfos,traitCategory}}}" }'
            json = self.fetchDataFromOpenTargets(query)
            self.extractAssociatedLeadVariantData(json)
            self.obtainTagVariantsForAssociatedLeadVariants()

        unique_variants = list(set(self.variants))

        for variant_id in unique_variants:
            variant_study_keys = list(set(self.variant_studies[variant_id]))
            if variant_study_keys:
                for key in variant_study_keys:
                    study_data = self.variant_study_data[key]
                    self.processStudyData(study_data)



    def obtainAssociatedLeadVariantsOnly(self):
        for id in self.human_ensembl_acc_ids:
            self.working_associated_lead_variant_set = []
            query ='{ "query": "{studiesAndLeadVariantsForGene(geneId: \\"' + id + '\\"){indexVariant{id}study {studyId,traitReported,traitEfos,traitCategory}}}" }'
            json = self.fetchDataFromOpenTargets(query)
            self.extractAssociatedLeadVariantData(json)

        unique_variants = list(set(self.variants))

        for variant_id in unique_variants:
            variant_study_keys = list(set(self.variant_studies[variant_id]))
            if variant_study_keys:
                for key in variant_study_keys:
                    study_data = self.variant_study_data[key]
                    self.processStudyData(study_data)




    def obtainTagVariantsForAssociatedLeadVariants(self):
        unique_working_set = list(set(self.working_associated_lead_variant_set))
        for variant in unique_working_set:
            query ='{ "query": "{tagVariantsAndStudiesForIndexVariant(variantId: \\"' + variant + '\\"){associations{tagVariant{id} study{studyId,traitReported,traitEfos,traitCategory}}}}" }'
            json = self.fetchDataFromOpenTargets(query)
            self.extractTagVariantsData(json)
    

    def obtainColocalisedStudies(self):
        for id in self.human_ensembl_acc_ids:
            query ='{ "query": "{colocalisationsForGene(geneId: \\"' + id + '\\"){study {studyId,traitReported,traitEfos,traitCategory}}}" }'
            json = self.fetchDataFromOpenTargets(query)
            self.extractAndProcessColocalisedStudyData(json)





    def fetchDataFromOpenTargets(self,query):
        
        url = 'http://genetics-api.opentargets.io/graphql'
        
        headers = {'Content-type': 'application/json',
                   'cache-control': 'no-cache'}
        
        response = requests.post(url, headers=headers, data=query)
        
        response.raise_for_status()
        return response.json()





    def extractAssociatedLeadVariantData(self, json):

        lead_variants = json['data']['studiesAndLeadVariantsForGene']

        for variantStudy in lead_variants:

            id = variantStudy['indexVariant']['id']
            study_data = variantStudy['study']

            if id:
                self.recordAssociatedLeadVariantId(id)
                self.recordStudyData(id,study_data)
            



    def extractTagVariantsData(self,json):

        tag_variants = json['data']['tagVariantsAndStudiesForIndexVariant']['associations']

        for variantStudy in tag_variants:
            
            id = variantStudy['tagVariant']['id']
            study_data = variantStudy['study']

            if id:
                self.recordTagVariantId(id)
                self.recordStudyData(id,study_data)



    def extractAndProcessColocalisedStudyData(self, json):

        colocalised_study = json['data']['colocalisationsForGene']

        for variantStudy in colocalised_study:

            self.processStudyData(variantStudy['study'])






    def recordAssociatedLeadVariantId(self,id):
        self.variants.append(id)
        self.associated_lead_variants.append(id)
        self.working_associated_lead_variant_set.append(id)


    def recordStudyData(self,variant_id,study_data):

        if study_data is not None:

            studyId = study_data['studyId']

            variant_study_key = variant_id + '---' + studyId

            if variant_study_key not in self.variant_study_data:
                self.variant_study_data[variant_study_key] = study_data
        
                if variant_id in self.variant_studies:
                    self.variant_studies[variant_id].append(variant_study_key)            
                else:
                    self.variant_studies[variant_id] = [variant_study_key]


    
    def addItemToDictionary(self,item,dictionary):
        if item not in dictionary:
            dictionary[item] = 1
        else:
            dictionary[item] = dictionary[item] + 1



    def recordTagVariantId(self,id):
        self.variants.append(id)
        self.tag_variants.append(id)



    def processStudyData(self,study):

        self.recordStudyId(study['studyId'])
        self.recordTrait(study['traitReported'])
        self.recordEfos(study['traitEfos'])
        self.recordTraitCategory(study['traitCategory'])


    
    def recordStudyId(self,id):
        self.studies.append(id)


    def recordTrait(self,trait):
        self.summary.addReportedTrait(trait)


    def recordEfos(self,efos):
        self.summary.addEfos(efos)


    def recordTraitCategory(self,category):
        self.summary.addTraitCategory(category)





if __name__ == '__main__':
    data = DataLoader( {'file': 'abnormal_heart_morphology.tsv'} )
    data.readData()
    mouse = ValidateMouseGenes( {'mouse_genes':data.uniqueGenes() } )
    mouse.fetchIds()
    orthologs = HumanOrthologs( {'mouse_data': mouse.mouse_data } )

    #orthologs.findOneToOneOrthologsAboveSupportCountThreshold(5)
    #orthologs.summariseOrthologAssignmentIssues()
    #orthologs.reportMouseOrthologMapping()

    #orthologs.setMinimumSupportCountThreshold(6)
    #orthologs.findBestOrthologMatches()
    #orthologs.summariseOrthologAssignmentIssues()
    #orthologs.reportMouseOrthologMapping()

    orthologs.findOneToOneOrthologs()
    orthologs.summariseOrthologAssignmentIssues()
    orthologs.reportMouseOrthologMapping()

    #openTargetsAnalysis = Analysis( {'human_ensembl_acc_ids': [*orthologs.human_orthologs] } )
    #openTargetsAnalysis.processHumanGenes()

    openTargetsAnalysis = Analysis( {'human_ensembl_acc_ids': [*orthologs.human_orthologs] } )
    openTargetsAnalysis.processHumanGenes()
